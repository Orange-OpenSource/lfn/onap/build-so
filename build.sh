#!/bin/bash
# SPDX-license-identifier: Apache-2.0

set -o errexit
set -o nounset
set -o pipefail

# install docker ce cli
apt-get update

apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -

add-apt-repository -y \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"

apt-get update

apt-get install -y docker-ce-cli="5:19.03.11~3-0~debian-buster"

# put the right setting for maven
mkdir -p /root/.m2
cp m2_settings.xml /root/.m2/settings.xml

# clone SO and retrieve gerrit review
git clone "${SO_GIT}"
cd so
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
git pull --no-edit "${SO_GIT}" \
  "refs/changes/${GERRIT_REVIEW: -2}/${GERRIT_REVIEW}/${GERRIT_PATCHSET}"

echo "_____________________________ Results ______________________"
# build the needed jars
mvn --no-transfer-progress  --quiet clean install -U -DskipTests \
    -Dmaven.javadoc.skip=true -Dadditionalparam=-Xdoclint:none -P docker \
    2>/dev/null | grep -v "\[parsing" | grep -v "\[search" | grep -v "\[loading" |
    grep -v "\[wrote" | grep -v "\[WARNING]" | grep -v "^  "
echo "____________________________________________________________"

# Push the containers
docker images
export DOCKERS=$(docker images | grep -v SNAPSHOT | grep onap/so | grep latest | cut -d' '  -f1)

docker login -u gitlab-ci-token -p "$CI_BUILD_TOKEN" "$CI_REGISTRY"

for docker in $DOCKERS
do
  export component=$(echo $docker | cut -d: -f1 | grep -o '[^/]*$')
  echo "retagging $docker:latest as ${component}:${GERRIT_REVIEW}-${GERRIT_PATCHSET}"
  docker tag $docker:latest ${REGISTRY}/${component}:${GERRIT_REVIEW}-${GERRIT_PATCHSET}
  docker push ${REGISTRY}/${component}:${GERRIT_REVIEW}-${GERRIT_PATCHSET}
done
